<?php

namespace Drupal\content_templates\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting content template entities.
 *
 * @ingroup content_template
 */
class ContentTemplateDeleteForm extends ContentEntityDeleteForm {}
